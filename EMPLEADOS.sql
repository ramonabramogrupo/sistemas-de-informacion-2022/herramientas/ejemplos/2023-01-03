﻿DROP DATABASE IF EXISTS empleados;
CREATE DATABASE empleados;

SET NAMES 'utf8';

USE empleados;

CREATE TABLE empleados (
  Nro int(11) NOT NULL,
  Nombre varchar(25) DEFAULT NULL,
  Fecha datetime DEFAULT NULL,
  Barrio varchar(12) DEFAULT NULL,
  TipoTrabajo varchar(12) DEFAULT NULL,
  HorasTrabajadas int(11) NOT NULL,
  DiasTrabajados int(11) NOT NULL,
  oficios varchar(25) DEFAULT NULL,
  PRIMARY KEY (Nro)
);

INSERT INTO empleados VALUES
(4, 'Suárez Luis', '1956-08-10 00:00:00', 'Cordon', 'Jornalero', 276, 0, 'Chofer'),
(7, 'Pérez Ema', '1961-04-09 00:00:00', 'Aguada', 'Efectivo', 0, 22, 'Azafata'),
(14, 'Pereira Ana', '1979-06-04 00:00:00', 'Aguada', 'Efectivo', 0, 21, 'Administración'),
(20, 'Gutiérrez Adolfo', '1965-07-14 00:00:00', 'Aguada', 'Jornalero', 128, 0, 'Chofer'),
(26, 'López Sofía', '1966-12-21 00:00:00', 'Unión', 'Jornalero', 490, 0, 'Azafata'),
(31, 'Alfonso Rodrigo', '1982-08-11 00:00:00', 'Cordon', 'Efectivo', 0, 22, 'Chofer'),
(32, 'Peña Beatriz', '1974-10-07 00:00:00', 'Unión', 'Efectivo', 0, 22, 'Ventas'),
(33, 'Acosta Pedro', '1978-07-30 00:00:00', 'Unión', 'Efectivo', 0, 18, 'Chofer'),
(38, 'Costela Martha', '1959-05-05 00:00:00', 'Cordon', 'Jornalero', 260, 0, 'Azafata'),
(40, 'Perera Leoncio', '1970-09-18 00:00:00', 'Unión', 'Efectivo', 0, 22, 'Administración'),
(43, 'Baccio Daniel', '1976-08-13 00:00:00', 'Blanqueada', 'Jornalero', 168, 0, 'Chofer'),
(46, 'López Lorena', '1962-12-08 00:00:00', 'Unión', 'Efectivo', 0, 22, 'Chofer'),
(47, 'Hernández Julio', '1969-07-19 00:00:00', 'Blanqueada', 'Efectivo', 0, 16, 'Ventas'),
(52, 'García Pedro', '1949-08-05 00:00:00', 'Blanqueada', 'Efectivo', 0, 18, 'Taller'),
(57, 'Costa Julian', '1959-01-26 00:00:00', 'Blanqueada', 'Jornalero', 450, 0, 'Chofer'),
(59, 'Martínez Soña', '1969-02-28 00:00:00', 'Aguada', 'Efectivo', 0, 26, 'Administración'),
(61, 'García Pablo', '1977-01-20 00:00:00', 'Unión', 'Efectivo', 0, 15, 'Taller'),
(69, 'Ferreira Federico', '1968-09-05 00:00:00', 'Aguada', 'Efectivo', 0, 20, 'Chofer'),
(72, 'Vicente Maria', '1973-05-09 00:00:00', 'Blanqueada', 'Jornalero', 120, 0, 'Azafata'),
(78, 'Peña Irma', '1980-09-03 00:00:00', 'Unión', 'Efectivo', 0, 22, 'Ventas'),
(81, 'Galo Rodolfo', '1968-11-10 00:00:00', 'Unión', 'Efectivo', 0, 22, 'Chofer'),
(84, 'Herrera Santiago', '1960-08-06 00:00:00', 'Centro', 'Jornalero', 176, 0, 'Chofer'),
(85, 'Giménez Elbio', '1978-12-27 00:00:00', 'Unión', 'Jornalero', 168, 0, 'Chofer'),
(86, 'Bresia Julia', '1982-01-06 00:00:00', 'Blanqueada', 'Efectivo', 0, 19, 'Administración'),
(95, 'Barcia Jouaquin', '1976-06-04 00:00:00', 'Blanqueada', 'Efectivo', 0, 10, 'Chofer'),
(98, 'Palacios Estela', '1969-05-25 00:00:00', 'Unión', 'Efectivo', 0, 21, 'Azafata'),
(105, 'Rodas Leonardo', '1975-04-18 00:00:00', 'Centro', 'Jornalero', 265, 0, 'Chofer'),
(113, 'Angelucci Fabricio', '1981-12-30 00:00:00', 'Blanqueada', 'Efectivo', 0, 22, 'Administración'),
(123, 'De Los Santos Jacinto', '1945-04-16 00:00:00', 'Aguada', 'Efectivo', 0, 22, 'Chofer');

